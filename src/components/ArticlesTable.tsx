import { useEffect } from "react";
import { DataGrid, GridColumns, useGridSlotComponentProps } from '@mui/x-data-grid';
import { useAppDispatch, useAppSelector } from "../app/hooks"
import { getInitialArticlesAsync, queryArticles, selectArticles } from "../features/articlesSlice";
import { Button, makeStyles } from "@material-ui/core";
import Pagination from '@mui/material/Pagination';

interface TableProps {
  keyword: string
}

const columns: GridColumns = [
  {
    field: "id",
    hide: true,
  },
  {
    field: "urlToImage",
    headerName: "Image",
    width: 110,
    sortable: false,
    editable: false,
    hide: false,
    renderCell: (params: any) => (
      <div style={{ paddingTop: 10, height: "100%" }}>
        <img
          src={params.value}
          style={{ width: 80, height: 70, objectFit: "cover" }}
          alt="thumbnail"
        />
      </div>
    ),
  },
  {
    field: "source",
    headerName: "Source",
    width: 200,
    hide: false,
    editable: false,
    sortable: false,
    renderCell: (params: any) => <div>{params.value.name}</div>,
  },
  {
    field: "author",
    headerName: "Author",
    sortable: false,
    width: 110,
    hide: false,
  },
  {
    field: "title",
    headerName: "Title",
    sortable: false,
    hide: false,
    width: 300,
    editable: false,
  },
  {
    field: "publishedAt",
    headerName: "Date",
    sortable: true,
    hide: false,
    editable: false,
    width: 140,
    sortComparator: (v1: any, v2: any, param1, param2) => {
      return new Date(v1).valueOf() - new Date(v2).valueOf()
    },

    renderCell: (params: any) => (
      <div>{new Date(params.value).toLocaleDateString()}</div>
    ),
  },
  {
    field: "url",
    headerName: "Url",
    sortable: false,
    hide: false,
    width: 110,
    renderCell: (params: any) => (
      <div>
        <Button variant="contained" href={params.value}>
          Details
        </Button>
      </div>
    ),
  },
];

const useStyles = makeStyles({
  root: {
    display: 'flex',
  },
});

function CustomPagination() {
  const { state, apiRef } = useGridSlotComponentProps();
  const classes = useStyles();

  return (
    <Pagination
      className={classes.root}
      color="primary"
      count={state.pagination.pageCount}
      page={state.pagination.page + 1}
      onChange={(event, value) => apiRef.current.setPage(value - 1)}
    />
  );
}


const ArticlesTable = (props: TableProps) => {
  const dispatch = useAppDispatch();
  const articles = useAppSelector(selectArticles)

  useEffect(() => {
    dispatch(getInitialArticlesAsync());
  }, []);

  useEffect(() => {
    dispatch(queryArticles(props.keyword));
  }, [props.keyword])

  return (
    <DataGrid
      rows={articles}
      columns={columns}
      pageSize={10}
      rowHeight={80}
      disableSelectionOnClick
      disableColumnMenu
      components={{
        Pagination: CustomPagination
      }}
    />
  )
}

export default ArticlesTable