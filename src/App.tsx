import React, { useState } from 'react';
import Box from '@mui/material/Box';
import { Grid, TextField } from '@mui/material';

import ArticlesTable from './components/ArticlesTable';

export default function App() {
  const [keyword, setKeyword] = useState('')

  return (
    <Grid container spacing={2} width={"80vw"} margin={"auto"} paddingTop={10}>
      <Grid item xs={12}>
        <TextField
          fullWidth
          label="Enter search keyword in title or description"
          id="fullWidth"
          value={keyword}
          onChange={(e: React.ChangeEvent<HTMLInputElement>) => {
            setKeyword(e.target.value)
          }}
        />
      </Grid>
      <Grid item xs={12} height={950}>
        <ArticlesTable keyword={keyword} />
      </Grid>
    </Grid>
  );
}
